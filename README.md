# GenetLoupExplorer

Cette interface a été développée dans le cadre du projet Liens pour le Parc national des Ecrins, en partenariat avec l'OFB. Elle présente les indices de présence du loup depuis 2008, à travers une cartographie interactive réalisée avec R Shiny et Leaflet.

Pour plus d'informations sur l'origine des données, voir "À propos" dans l'application.

## Fonctionnement de l'interface

L'interface est très simple. Elle permet de varier la période pour laquelle les données sont représentées (en changeant l'année de départ et la durée), et de filtrer les individus en fonction du nombre minimum d'indices.

# Structure des données

L'application suppose l'existence des fichiers suivants dans le dossier genetloupexplorer/www/data

- PNE.shp: shapefile avec les contours du Parc national des Ecrins
- pne_extended10b_cleaned.csv: Données Spatiales d'indices
- PNE_genotype_REF_extended10.xls: Excel file with genetics information (microsatellite data)


## Données Spatiales d'indices

### Fichier

pne_extended10b_cleaned.csv

### Description

Localisations des indices qui ont été soumise a analyse genetique. 1 Ligne = Une indice.

### Source

OFB, Christophe Duchamp
Envoyé le lundi 6 Mars 2023 15:50:39 à Nathan Daumergue (nommé "pne_extended10b.csv" )

### Structure fichier

Le fichier est lu par la fonction read.csv2, c'est-à-dire que les colonnes sont supposées être séparées par des ';', et le signe décimal est supposé être ','. L'encodage du fichier est supposé être UTF-8.

Les colonnes suivantes doivent être présentes (leur ordre n'est pas important) :

- date : Date de l'analyse, in format "day/month/year"", e.g. "13/03/2001" 
- X : Coordonnée X en L93
- Y : Coordonnée X en L93
- GENOTYPE_T : Id du genotype dans la table des données genetiques 

## Données Genotypes

### Fichier 

PNE_genotype_REF_extended10.xls

### Source

OFB, Christophe Duchamp
Envoyé le lundi 6 Mars 2023 15:50:39 à Nathan Daumergue

### Structure fichier

Les colonnes suivantes doivent être présentes (leur ordre n'est pas important) :

- GENOTYPE_T : Identifiant du loup S = session numero de session - ordre d'apparition des genotypes)
- IQ : indice chiffé regle OFB
    - \> 0.7 : fiable
    - 0.5 - 0.7 : toléré 
    - < 0.5 : A ne pas utiliser
- 22 colonnes avec information sur des microsatellites. Les noms de colonnes des microsatellites (et que ces colonnes) doit commencer avec deux chiffres: e.g. 01-AHT103 ou 07-C09173. Les valuers des microsatellites sont codé en 6 chiffre correspondent a 2 locus de 3 chiffres: un qui vient du pere 1 de la mere (ordre non foralisé).

