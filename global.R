library(shiny)
library(sf)
library(tidyr)
library(dplyr)
library(lubridate)
library(leaflet)
library(ctmm)
library(scales)
library(leafem)
library(dendextend)
library(ggplot2)
library(adegenet)
library(shinyjs)

# Helper functions
#' Separate columns representing alleles
#'
#' Columns are supposed to be made up of 6 values.
#'
#' @param x A data frame
#' @param cols A vector with columnames
#' @return A data frame
#' @export
separate_columns <- function(x, cols) {
  stopifnot(all(cols %in% colnames(x)))
  for (i in cols) {
    x <- x |> tidyr::separate(col = i, remove = TRUE,
                              into = paste0(i, "_", c("a", "b")),
                              sep = 3)
  }
  x
}

#' Extract column names that represent loci
#'
#' @param x A data frame
#' @param regx A regular expression identifying column names that represent loci
#' @return A vector with names representing loci
#' @export
get_loci_names <- function(x, regx = "^[0-9][0-9]") {
  n <- colnames(x)
  n[grep(regx, n)]
}


#' Filter genotypes
#'
#' @param x A data frame
#' @param min_IQ Minimum quality index
#' @param min_OK Minimum number of alleles
#' @return A data frame with genotypes
#' @export
filter_genotypes <- function(x, min_IQ = 0.5, min_OK = 16) {
  x %>% separate_columns(get_loci_names(x)) %>%
    dplyr::filter(IQ >= min_IQ) %>%
    dplyr::filter(rowSums(dplyr::select(., dplyr::matches("^[0-9][0-9]")) != "000") >= min_OK)
}

#' ## Cluster genotypes
#'
#' @param x Data frame with genotypes
#' @param do_pcoa Do a PCOA
#' @export
cluster_genotypes <- function(x, do_pcoa = TRUE) {
  pegasv <- pegas::alleles2loci(x |> dplyr::select(dplyr::matches("^[0-9][0-9]")))
  colnames(pegasv) <- gsub("\\.", "_", gsub("_a", "", colnames(pegasv)))
  testv <- pegas::loci2genind(pegasv, ploidy=2)
  d <- stats::dist(scale(testv)) # factoextra::get_dist(testv, stand = TRUE, method = "euclidean")
  if (do_pcoa) {
    res.pcoa <- ade4::dudi.pco(d = d, scannf = FALSE, nf = 3) # Why do we do this dimension reduction?
    d <- dist(res.pcoa$li)# factoextra::get_dist(res.pcoa$li, method="euclidean")
  }
  h <- stats::hclust(d, method="ward.D2")
  h$labels <- x$GENOTYPE_T

  res <- list("pcoa" = ifelse(do_pcoa, res.pcoa, NA),
              "d" = d,
              "h" = h,
              "GENOTYPE_T" = x$GENOTYPE_T)
  res
}

#' Genotypes present in a given period defined by start year and duration
#'
#' @param recaptures Data frame with recaptures
#' @param genotypes Data frame with genotype information
#' @param start_year Start year
#' @param duration Number of years to group together
#' @param cluster_by_period Whether to cluster for each period
#' @param k_fullperiod Number of clusters for the full period
#' @param varname Variable name for linking recaptures and genotypes
#' @return A data frame
#' @export
period_genotypes <- function(recaptures, genotypes, start_year,
                             duration, cluster_by_period,
                             k_fullperiod, varname = "GENOTYPE_T") {
  selected_years <- seq(start_year, by = 1, length.out = duration)
  if (cluster_by_period) {
    genotypes_period <- recaptures |> as.data.frame() |> dplyr::filter(year %in% selected_years) |> dplyr::select(GENOTYPE_T) |> unique()
    genotypes_period <- dplyr::inner_join(genotypes_period, genotypes, by = varname)
  } else {
    genotypes_period <- genotypes
  }
  cluster_period <- cluster_genotypes(genotypes_period)
  genotypes_period$k <- stats::cutree(cluster_period$h, k = k_fullperiod) # or dendextend::cutree?
  genotypes_period
}

#' Simplifies list to data frame
#'
#' @param x A list
#' @return A data frame
#' @export
as_table <- function(x) {sapply(x, function(z) z)}

#' Transforms data to telemetry format
#'
#' @param x A data frame (sf points)
#' @return A telemetry object
#' @export
as_telemetry <- function(x) {
  # convert data to "telemetry" class used in ctmm
  xorig <- x
  x <- cbind(as.data.frame(x), sf::st_coordinates(x))
  x$timestamp <- x$Date
  x$x <- x$X
  x$y <- x$Y
  x$individual.local.identifier <- as.character(x$GENOTYPE_T)
  lonlat <- sf::st_coordinates(sf::st_transform(xorig, crs= 4326))
  colnames(lonlat) <- c("lon", "lat")
  x <- cbind(x, lonlat)
  ctmm::as.telemetry(x)
}

#' Generate random colors, the number corresponds to unique values in x
#'
#' @param x A vector
#' @return A color vector
#' @export
random_color <- function(x) {
  x <- unique(x)
  col <- sample(scales::hue_pal()(length(x)))
  names(col) <- x
  col
}

#' Get the centroid of a bounding box, in lat-lon
#'
#' @param x An sf object
#' @return Lat-lon coordinates of centroid of x
#' @export
st_latlon_bbox_centroid <- function(x) {
  e <- sf::st_as_sfc(sf::st_bbox(x), crs = sf::st_crs(x))
  e <- sf::st_transform(e, 4326)
  sf::st_coordinates(sf::st_centroid(e))
}

data_path <- "www/data"
genotype_file <- normalizePath(file.path(data_path, "PNE_genotype_REF_extended10.xls"))
region_shape_file <- normalizePath(file.path(data_path, "PNE.shp"))
genotype_locations_file <- normalizePath(file.path(data_path, "pne_extended10b_cleaned.csv"))
min_IQ <- 0.5

#' Import data
genotypes <- readxl::read_excel(genotype_file)
ZE <- sf::read_sf(region_shape_file)
ZE <- sf::st_transform(ZE, 4326)
bbox_ze <- sf::st_bbox(ZE)
recaptures <- read.csv2(genotype_locations_file, fileEncoding = 'UTF-8')
recaptures <- sf::st_as_sf(recaptures[complete.cases(recaptures[,c("X", "Y")]),], coords = c("X", "Y"), crs = 2154)
recaptures <- sf::st_transform(recaptures, 4326)
# Add column "Date" with class "Date" (instead of integer, character or factor)
recaptures <- recaptures %>% dplyr::mutate(Date = dmy(date), year = year(Date))
year_start <- 2008
year_end <- max(recaptures$year)

#Select well-defined genotypes and generate separate columns for alleles
genotypes_IQ <- filter_genotypes(genotypes)
# Keep only observations within the PNE
recaptures_within_studyzone <- recaptures %>% dplyr::filter(st_within(., ZE) %>% lengths > 0)

#' Select recaptures of individuals with at least one recapture in the ZE.
recaptures_studyzone <- dplyr::filter(recaptures, GENOTYPE_T %in% unique(recaptures_within_studyzone$GENOTYPE_T))

#' genotypes_full_period contains the genotypes with a quality index of 0.5 of all individuals that were
#' observed at least once in the study area during the study period.
genotypes_studyzone_full_period <- recaptures_studyzone %>% as.data.frame() %>%
  dplyr::filter(year >= year_start & year <= year_end) %>% dplyr::select(GENOTYPE_T) %>% unique()
genotypes_studyzone_full_period <- dplyr::inner_join(genotypes_studyzone_full_period, genotypes_IQ, by = "GENOTYPE_T")

set.seed(2)
color_id <- random_color(genotypes_studyzone_full_period$GENOTYPE_T)
color_id_region <- random_color(genotypes_studyzone_full_period$GENOTYPE_T)

#' Keep only recaptures of genotypes with high genotype quality
recaptures_studyzone_full_period <- recaptures_studyzone %>%
  dplyr::filter(year >= year_start & year <= year_end & GENOTYPE_T %in%
                  genotypes_studyzone_full_period$GENOTYPE_T)
